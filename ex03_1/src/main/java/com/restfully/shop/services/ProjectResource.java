package com.restfully.shop.services;

import com.restfully.shop.domain.Project;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import us.codecraft.xsoup.Xsoup;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Path("/projects")
public class ProjectResource {
   private Map<Integer, Project> projectDB = new ConcurrentHashMap<Integer, Project>();
   private AtomicInteger idCounter = new AtomicInteger();

   public ProjectResource() {
   }
 
   @GET
   @Path("/{project}/meetings/{year}/")
   @Produces("application/xml")
   public StreamingOutput getProjectMeetings(@PathParam("project") final String projectName,
		   @PathParam("year") final String year) {
     return new StreamingOutput() {
	     public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	        PrintStream writer = new PrintStream(outputStream);
	        List<String> links = new ArrayList<String>();
	        String urlString2 = "http://eavesdrop.openstack.org/meetings/" + projectName + "/" + year;
        	URL url2 = new URL(urlString2);
        	try{
	        	URLConnection con2 = url2.openConnection();
	        	InputStream in2 = con2.getInputStream();
	        	String encoding2 = con2.getContentEncoding();
	        	encoding2 = encoding2 == null ? "UTF-8" : encoding2;
	        	String body2 = IOUtils.toString(in2, encoding2);
	        	
	        	Document doc2 = (Document) Jsoup.parse(body2);
	        	List<String> chunk = Xsoup.compile("//td[2]/a").evaluate((org.jsoup.nodes.Element) doc2).list();
	        	for (String name : chunk){        		
	        		String parsed = name.substring(name.indexOf(">") + 1);
	        		String parsed2 = parsed.substring(0, parsed.indexOf("<"));
	        		if(parsed2.equals("Parent Directory"))
	        			continue;
	        		
	        		String parsedLink = name.substring(name.indexOf("\"")+1);
	        		String parsedLink2 = parsedLink.substring(0, name.indexOf("\""));
	        		links.add(parsed2);
	        	}	   
        	} catch(Exception e) {
        		e.printStackTrace();
        	}
	        writer.printf("<project name='%s'>\n", projectName );
	        for(String link : links) {
	        	writer.printf("<link>%s</link>", link);
	        }
	        writer.println("</project>");
	     }   
     };
   }

   @GET
   @Path("/{project}/irclogs/")
   @Produces("application/xml")
   public StreamingOutput getIrcMeetings(@PathParam("project") final String projectName) {
     return new StreamingOutput() {
	     public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	        PrintStream writer = new PrintStream(outputStream);
	        List<String> links = new ArrayList<String>();
	        String modProjName = projectName;
	        if(modProjName.startsWith("#"))
	        	modProjName = "%23" + projectName.substring(1);
	        String urlString1 = "http://eavesdrop.openstack.org/irclogs/" + modProjName + "/";
	        System.out.println(urlString1);
        	URL url = new URL(urlString1);
        	try{
	        	URLConnection con = url.openConnection();
	        	InputStream in = con.getInputStream();
	        	String encoding = con.getContentEncoding();
	        	encoding = encoding == null ? "UTF-8" : encoding;
	        	String body = IOUtils.toString(in, encoding);
	        	
	        	Document doc = (Document) Jsoup.parse(body);
	        	//System.out.println(doc);
	        	List<String> chunk = Xsoup.compile("//td[2]/a").evaluate((org.jsoup.nodes.Element) doc).list();
	        	System.out.println(chunk);
	        	for (String name : chunk){        		
	        		String parsed = name.substring(name.indexOf(">") + 1);
	        		String parsed2 = parsed.substring(0, parsed.indexOf("<"));
	        		if(parsed2.equals("Parent Directory"))
	        			continue;
	        		
	        		String parsedLink = name.substring(name.indexOf("\"")+1);
	        		String parsedLink2 = parsedLink.substring(0, name.indexOf("\""));
	        		links.add(parsed2);
	        	}	   
	        	//System.out.println(chunk);
	        	System.out.println(links);
        	} catch(Exception e) {
        		e.printStackTrace();
        	}
	        writer.printf("<project name='%s'>\n", projectName );
	        for(String link : links) {
	        	writer.printf("<link>%s</link>", link);
	        }
	        writer.println("</project>");
	     }   
     };
   }
   
   @GET
   @Path("/")
   @Produces("application/xml")
   public StreamingOutput getProjects() {
     return new StreamingOutput() {
	     public void write(OutputStream outputStream) throws IOException, WebApplicationException {
	        PrintStream writer = new PrintStream(outputStream);
	        Set<String> proj_names = new HashSet<String>();
	        writer.println("<projects>");
	        
	        	String urlString = "http://eavesdrop.openstack.org/irclogs/";
	        	String urlString2 = "http://eavesdrop.openstack.org/meetings/";
	        	URL url = new URL(urlString);
	        	URL url2 = new URL(urlString2);
	        	URLConnection con = url.openConnection();
	        	URLConnection con2 = url2.openConnection();
	        	InputStream in = con.getInputStream();
	        	InputStream in2 = con2.getInputStream();
	        	String encoding = con.getContentEncoding();
	        	String encoding2 = con2.getContentEncoding();
	        	encoding = encoding == null ? "UTF-8" : encoding;
	        	encoding2 = encoding2 == null ? "UTF-8" : encoding2;
	        	String body = IOUtils.toString(in, encoding);
	        	String body2 = IOUtils.toString(in2, encoding2);
	        	
	        	Document doc = (Document) Jsoup.parse(body);
	        	List<String> irc_names = Xsoup.compile("//td[2]/a").evaluate((org.jsoup.nodes.Element) doc).list();
	        	for (String name : irc_names){
	        		String parsed = name.substring(name.indexOf(">") + 1);
	        		String parsed2 = parsed.substring(0, parsed.indexOf("<"));
	        		if(parsed2.equals("Parent Directory"))
	        			continue;
	        		proj_names.add(parsed2);
	        	}
	        	Document doc2 = (Document) Jsoup.parse(body2);
	        	List<String> meeting_names = Xsoup.compile("//td[2]/a").evaluate((org.jsoup.nodes.Element) doc2).list();
	        	for (String name : meeting_names){
	        		String parsed = name.substring(name.indexOf(">") + 1);
	        		String parsed2 = parsed.substring(0, parsed.indexOf("<"));
	        		if(parsed2.equals("Parent Directory"))
	        			continue;
	        		proj_names.add(parsed2);
	        	}	   

	        	for (String name : proj_names) {
	        		writer.printf("<project>%s</project>\n",name);
	        	}

	        writer.println("</projects>");
	     }   
     };
   }
   





}
