package com.restfully.shop.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/myeavesdrops")
public class EavesdropApplication extends Application {
   private Set<Object> singletons = new HashSet<Object>();

   public EavesdropApplication() {
      singletons.add(new ProjectResource());
   }

   @Override
   public Set<Object> getSingletons() {
      return singletons;
   }
}
